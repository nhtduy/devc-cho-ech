from django.db import models

class User(models.Model):
  # ids = models.IntegerField(primary_key=True)
  user_session = models.CharField(max_length=255,null=True)
  user_fingerprint = models.CharField(max_length=255,null=True)
  page_platform = models.CharField(max_length=255,null=True)
  page_number = models.CharField(max_length=255,null=True)
  event_type = models.CharField(max_length=255,null=True)
  event_name = models.CharField(max_length=255,null=True)
  event_server_time = models.TimeField(max_length=255,null=True)
  filter_keyword = models.CharField(max_length=255,null=True)
  filter_category_id = models.IntegerField(null=True)
  filter_region_id = models.IntegerField(null=True)
  ad_list_id = models.IntegerField(null=True)
  adposition = models.IntegerField(null=True)
  adplacement = models.CharField(max_length=255,null=True)
  adsource = models.CharField(max_length=255,null=True)
  filter_type = models.CharField(max_length=255,null=True)
  filter_price = models.IntegerField(null=True)
  filter_brand = models.CharField(max_length=255,null=True)
  filter_model = models.CharField(max_length=255,null=True)
  lead_type = models.CharField(max_length=255,null=True)
  
# Create your models here.
