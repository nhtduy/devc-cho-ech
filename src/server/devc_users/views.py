from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import F,ExpressionWrapper,FloatField,Count
import pandas as pd
import json
from django.db.models.functions import Floor
from .models import User#,Category
from .serializers import UserSerializer#,CategorySerializer
from datetime import datetime
import math
class UserAction(APIView):
    def post(self,request,finger_id,ad_id,event_name):
        print(finger_id)
        print(ad_id)
        print(event_name)
        print("Current time:",datetime.now())
        new_event = User.objects.create(user_fingerprint=finger_id,ad_list_id=ad_id,event_name=event_name,event_server_time=datetime.now())
        new_event.save()
        return Response({"success": "User's Event '{}' created successfully".format(new_event.user_fingerprint)})
class ItemCategories(APIView):
    def get(self,request,cate_id):
        if(cate_id==-1):
            print(cate_id)
            category_id_list = ['1','11','12','13','2','3','4','5','6','7','8','9']
            category_name_list = ['PTY','Mom and kids','Animals and equipments','Career','Vehicles','Equipments','Entertainments','Electronic Devices','Services and Travels','Others','Office','Household Machine']
            category_dict_list = []
            for (category_id,category_name) in zip(category_id_list,category_name_list):
                has_child = True
                if(category_name=="Mom and kids" or category_name=="Others"):
                    has_child = False
                category_dict_list.append({'category_id':category_id,'category_name':category_name,'has_child':has_child}) 
            response_categories = json.dumps(category_dict_list)
            # category = pd.DataFrame.from_records(Item.objects.all().values())
            # category = category.head()
            return Response({"category": category_dict_list})
        else:
            tmp = Item.objects.filter(mother_category_id=cate_id).order_by('category').values('category','category_name').distinct()
            # tmp= tmp.head()
            tmp = list(tmp)
            for i in range(len(tmp)):
                tmp[i]['has_child']=False
                tmp[i]['category_id']=tmp[i].pop('category')
            print(tmp)
            return Response({"category": tmp})
class ItemListing(APIView):
    def get(self,request,cate_id,page_number,record):
        # print(id_ad)
        print("Im here")
        if(cate_id<1000):
            #list mother_cat_items
            total_num_items = len(Item.objects.filter(mother_category_id=cate_id))
        else:
            #list category_items
            print("sadsa")
        #items = Item.objects.get(pk=1)
        # the many param informs the serializer that it will be serializing more than a single article.
        # serializer = ItemSerializer(items)
        return Response({"items": total_num_items})
# Create your views here.
