from django.apps import AppConfig


class DevcUsersConfig(AppConfig):
    name = 'devc_users'
