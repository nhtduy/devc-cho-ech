from rest_framework import serializers
from .models import User

class UserSerializer(serializers.Serializer):
    # ids = serializers.CharField(max_length=255)
    user_session = serializers.CharField(max_length=255)
    user_fingerprint = serializers.CharField(max_length=255)
    page_platform = serializers.CharField(max_length=255)
    page_number = serializers.CharField(max_length=255)
    event_type = serializers.CharField(max_length=255)
    event_name = serializers.CharField(max_length=255)
    event_server_time = serializers.TimeField()
    filter_keyword = serializers.CharField(max_length=255)
    filter_category_id = serializers.IntegerField()
    filter_region_id = serializers.IntegerField()
    ad_list_id = serializers.IntegerField()
    adposition = serializers.IntegerField()
    adplacement = serializers.CharField(max_length=255)
    adsource = serializers.CharField(max_length=255)
    filter_type = serializers.CharField(max_length=255)
    filter_price = serializers.IntegerField()
    filter_brand = serializers.CharField(max_length=255)
    filter_model =serializers.CharField(max_length=255)
    lead_type = serializers.CharField(max_length=255)
    #author_id = serializers.IntegerField()

    # def create(self, validated_data):
    #     return Article.objects.create(**validated_data)
# class CategorySerializer(serializers.Serializer):
#     category_name = serializers.CharField(max_length=255)
#     category_id = serializers.IntegerField()