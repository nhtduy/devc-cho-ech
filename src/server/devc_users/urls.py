from django.urls import path

from .views import UserAction


app_name = "items"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    #api getting ad with id_ad params
    # path('users/<char:fingerprint>/page')
    # path('items/<int:id_ad>/', ItemView.as_view()),
    # #api getting list of categories
    # path('categories/',ItemCategories.as_view(),{'cate_id':-1}),
    # path('categories/<int:cate_id>',ItemCategories.as_view()),
    path('users/<str:finger_id>/ad/<int:ad_id>/event/<str:event_name>/',UserAction.as_view())
    # path('categories/<int:cate_id>/page/<int:page_number>/record/<int:record>/',ItemListing.as_view())
]