from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import F,ExpressionWrapper,FloatField
import pandas as pd
import json
from django.db.models.functions import Floor
from .models import Item#,Category
from .serializers import ItemSerializer#,CategorySerializer
from django.core import serializers
import math
class ItemView(APIView):
    def get(self, request,id_ad):
        # id_ad = request.GET['id_ad']
        print(id_ad)
        items = Item.objects.filter(list_id=id_ad).values()
        items = list(items)
        return Response({"items": items})
class ItemCategories(APIView):
    def get(self,request,cate_id):
        if(cate_id==-1):
            print(cate_id)
            category_id_list = ['1','11','12','13','2','3','4','5','6','7','8','9']
            category_name_list = ['PTY','Mom and kids','Animals and equipments','Career','Vehicles','Equipments','Entertainments','Electronic Devices','Services and Travels','Others','Office','Household Machine']
            category_dict_list = []
            for (category_id,category_name) in zip(category_id_list,category_name_list):
                has_child = True
                if(category_name=="Mom and kids" or category_name=="Others"):
                    has_child = False
                category_dict_list.append({'category_id':category_id,'category_name':category_name,'has_child':has_child}) 
            response_categories = json.dumps(category_dict_list)
            # category = pd.DataFrame.from_records(Item.objects.all().values())
            # category = category.head()
            return Response({"category": category_dict_list})
        else:
            tmp = Item.objects.filter(mother_category_id=cate_id).order_by('category').values('category','category_name').distinct()
            # tmp= tmp.head()
            tmp = list(tmp)
            for i in range(len(tmp)):
                tmp[i]['has_child']=False
                tmp[i]['category_id']=tmp[i].pop('category')
            print(tmp)
            return Response({"category": tmp})
class ItemListing(APIView):
    def get(self,request,cate_id,page_number,record):
        # print(id_ad)
        page_number-=1
        if(cate_id<1000):
            total_num_items = Item.objects.filter(mother_category_id=cate_id).order_by("id")
            #list mother_cat_items
            if(page_number*record+record>=len(total_num_items)):
                items_list = total_num_items[page_number*record:]
            else:
                items_list = total_num_items[page_number*record:page_number*record+record]
        else:
            total_num_items = Item.objects.filter(category=cate_id).order_by("id")
            #list mother_cat_items
            if(page_number*record+record>=len(total_num_items)):
                items_list = total_num_items[page_number*record:]
            else:
                items_list = total_num_items[page_number*record:page_number*record+record]
            #list category_items
        #items = Item.objects.get(pk=1)
        # the many param informs the serializer that it will be serializing more than a single article.
        page_number+=1
        serializer = ItemSerializer(items_list,many=True)
        page = {'page': page_number, 'record': record,'all_page':math.floor((len(total_num_items)-1)/record)+1}
        return Response({"items:":serializer.data,"page": page})
class SearchKeywordView(APIView):
    def get(self,request,keyword_value,page_number,record):
        # print(id_ad)
        page_number-=1
        total_num_items = Item.objects.filter(subject__icontains=keyword_value).order_by("id")
        #list mother_cat_items
        if(page_number*record+record>=len(total_num_items)):
            items_list = total_num_items[page_number*record:]
        else:
            items_list = total_num_items[page_number*record:page_number*record+record]
        page_number+=1
        serializer = ItemSerializer(items_list,many=True)
        page = {'page': page_number, 'record': record,'all_page':math.floor((len(total_num_items)-1)/record)+1}
        return Response({"items:":serializer.data,"page": page,"keywords_search": keyword_value})





















        #serializer = CategorySerializer(category)
    # def post(self, request):
    #     article = request.data.get('article')

    #     # Create an article from the above data
    #     serializer = ArticleSerializer(data=article)
    #     if serializer.is_valid(raise_exception=True):
    #         article_saved = serializer.save()
    #     return Response({"success": "Article '{}' created successfully".format(article_saved.title)})
# Create your views here.
