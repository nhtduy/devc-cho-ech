# Generated by Django 2.2.7 on 2019-12-01 07:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0009_auto_20191201_1417'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='ad_id',
            field=models.BigIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='item',
            name='category',
            field=models.BigIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='item',
            name='list_id',
            field=models.BigIntegerField(null=True),
        ),
    ]
