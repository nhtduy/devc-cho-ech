from rest_framework import serializers
from .models import Item

class ItemSerializer(serializers.Serializer):
    # ids = serializers.CharField(max_length=255)
    ad_id = serializers.CharField(style={'base_template': 'textarea.html'})
    category = serializers.CharField(style={'base_template': 'textarea.html'})
    category_name = serializers.CharField(max_length=255)
    list_id = serializers.CharField(style={'base_template': 'textarea.html'})
    subject = serializers.CharField(max_length=255)
    number_of_images = serializers.CharField(max_length=255)
    price = serializers.CharField(max_length=255)
    company_ad = serializers.CharField(max_length=255)
    seller_type = serializers.CharField(max_length=255)
    on_listing = serializers.CharField(max_length=255)
    in_shop = serializers.CharField(max_length=255)
    city = serializers.CharField(max_length=255)
    district = serializers.CharField(max_length=255)
    ward = serializers.CharField(max_length=255)
    city_id = serializers.CharField(max_length=255)
    region = serializers.CharField(max_length=255)
    region_id = serializers.CharField(max_length=255)
    shop_id = serializers.CharField(max_length=255)
    address = serializers.CharField(max_length=255)
    street = serializers.CharField(max_length=255)
    mother_category_id = serializers.IntegerField()
    #author_id = serializers.IntegerField()

    # def create(self, validated_data):
    #     return Article.objects.create(**validated_data)
# class CategorySerializer(serializers.Serializer):
#     category_name = serializers.CharField(max_length=255)
#     category_id = serializers.IntegerField()