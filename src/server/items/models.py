from django.db import models

class Item(models.Model):
  # ids = models.IntegerField(primary_key=True)
  ad_id = models.BigIntegerField(null=True)
  category = models.BigIntegerField(null=True)
  category_name = models.CharField(max_length=255,null=True)
  list_id = models.BigIntegerField(null=True)
  subject = models.CharField(max_length=255,null=True)
  number_of_images = models.CharField(max_length=255,null=True)
  price = models.CharField(max_length=255,null=True)
  company_ad = models.CharField(max_length=255,null=True)
  seller_type = models.CharField(max_length=255,null=True)
  on_listing = models.CharField(max_length=255,null=True)
  in_shop = models.CharField(max_length=255,null=True)
  city = models.CharField(max_length=255,null=True)
  district = models.CharField(max_length=255,null=True)
  ward = models.CharField(max_length=255,null=True)
  city_id = models.CharField(max_length=255,null=True)
  region = models.CharField(max_length=255,null=True)
  region_id = models.CharField(max_length=255,null=True)
  shop_id = models.CharField(max_length=255,null=True)
  address = models.CharField(max_length=255,null=True)
  street = models.CharField(max_length=255,null=True)
  mother_category_id = models.IntegerField(null=True)
# class Category(models.Model):
#   category_name = models.CharField(max_length=255)
#   category_id = models.IntegerField()



