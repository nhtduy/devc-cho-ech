from django.urls import path

from .views import UserItemView


app_name = "users_items"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    #api getting ad with id_ad params
    path('users/<str:fingerprint>/saving/', UserItemView.as_view()),
    path('users/<str:fingerprint>/saving/<int:ad_id>/',UserItemView.as_view()),
    
]