from django.apps import AppConfig


class UsersItemsConfig(AppConfig):
    name = 'users_items'
