from rest_framework import serializers
from .models import UserItem

class UserItemSerializer(serializers.Serializer):
    # ids = serializers.CharField(max_length=255)
    user_fingerprint = serializers.CharField(max_length=255)
    ad_id = serializers.IntegerField()
    #author_id = serializers.IntegerField()

    # def create(self, validated_data):
    #     return Article.objects.create(**validated_data)
# class CategorySerializer(serializers.Serializer):
#     category_name = serializers.CharField(max_length=255)
#     category_id = serializers.IntegerField()