from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import F,ExpressionWrapper,FloatField
import pandas as pd
import json
from django.db.models.functions import Floor
from .models import UserItem#,Category
from django.forms.models import model_to_dict
from .serializers import UserItemSerializer#,CategorySerializer
import math
class UserItemView(APIView):
    def get(self,request, fingerprint):
        # id_ad = request.GET['id_ad']
        print(fingerprint)
        print("here")
        items_saving = UserItem.objects.filter(user_fingerprint=fingerprint)
        # the many param informs the serializer that it will be serializing more than a single article.
        serializer = UserItemSerializer(items_saving,many=True)
        return Response({"items_saving": serializer.data})
    def post(self,request,fingerprint,ad_id):
        print(fingerprint)
        print(ad_id)
        new_saving = UserItem.objects.create(user_fingerprint=fingerprint,ad_id=ad_id)
        new_saving.save()
        # if(serializer.is_valid(raise_exception=True)):
        #     userItem_saved = serializer.save()
        return Response({"success": "Saving '{}' created successfully".format(new_saving.user_fingerprint)})
    # def post(self, request):
    #     article = request.data.get('article')

    #     # Create an article from the above data
    #     serializer = ArticleSerializer(data=article)
    #     if serializer.is_valid(raise_exception=True):
    #         article_saved = serializer.save()
    #     return Response({"success": "Article '{}' created successfully".format(article_saved.title)})

