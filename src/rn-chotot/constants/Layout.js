import { Dimensions, StyleSheet } from 'react-native';
import Constants from 'expo-constants';
import colors from './Colors';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default styles = StyleSheet.create({
  window: {
    width,
    height,
  },
  row: {
    flexDirection: 'row'
  },
  isSmallDevice: width < 375,
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: Constants.statusBarHeight,
  },
  contentContainer: {
    paddingTop: 30,
  },
  viewSeeAll: {
    flexDirection: 'row', alignItems: 'center', marginTop: 6, marginBottom: 5,
    justifyContent: 'space-between', paddingRight: 10, paddingLeft: 17
  },
  textSee: {
    fontSize: 18,
    fontWeight: 'normal',
    lineHeight: 21
  },
  btnSeeAll: {
    backgroundColor: '#F99807',
    borderRadius: 8,
    paddingTop: 6,
    paddingBottom: 7,
    paddingHorizontal: 20
  },
  textSeeAll: {
    fontSize: 13,
    fontWeight: 'bold',
    lineHeight: 15
  },
  shadow: {
    elevation: 4,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 0.5,
    shadowColor: colors.black
  },
  titleHeight: {
    fontSize: 17,
    fontWeight: '500',
    color: colors.textColor
  },
  titleMedium: {
    fontSize: 15,
    fontWeight: '500',
    color: colors.textColor
  },
  textContentDetail: {
    fontSize: 14,
    fontWeight: 'normal'
  }
});
