const tintColor = '#2f95dc';

export default colors = {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  tabBarLabel: '#000',
  yellow: '#FFBA00',
  blackShadow: 'rgba(0,0,0,0.25)',
  black: '#000',
  white: '#fff',
  textColor: '#0A1034',
  btnDetail: '#FFA100'
};
