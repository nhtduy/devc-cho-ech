import React from 'react';
import HomeWhite from '../assets/svg/home-white.svg';
import HomeBlack from '../assets/svg/home-black.svg';
import Search from '../assets/svg/search.svg';
import SearchBold from '../assets/svg/search-bold.svg';
import UserWhite from '../assets/svg/user-white.svg';



export default function TabBarIcon(props) {

    var icon = <HomeWhite width={26} height={26} />

    switch (props.name) {
        case "Home":
            icon = <HomeWhite width={26} height={26} />
            break;
        case "Search":
            icon = <Search width={26} height={26} />
            break;
        case "Profile":
            icon = <UserWhite width={26} height={26} />
            break;
        default:
            icon = <HomeWhite width={26} height={26} />
    }

    if (props.focused) {
        switch (props.name) {
            case "Home":
                icon = <HomeBlack width={26} height={26} />
                break;
            case "Search":
                icon = <SearchBold width={26} height={26} />
                break;
            case "Profile":
                icon = <UserWhite width={26} height={26} />
                break;
            default:
                icon = <HomeWhite width={26} height={26} />
        }
    }
    return icon;
}
