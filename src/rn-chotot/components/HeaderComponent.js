import React from 'react';
import {
    View, Text, StyleSheet,
} from 'react-native';

const HeaderComponent = (props) => {

    var { title = 'Chợ Tốt' } = props;
    return (
        <View style={styles.header}>
            <Text style={styles.headerText}>
                {title}
            </Text>
        </View>
    )
}

export default HeaderComponent;
function elevationShadowStyle(elevation) {
    return {
        elevation,
        shadowColor: 'rgba(0,0,0,0.25)',
        shadowOffset: { width: 0, height: elevation },
        shadowOpacity: 0.3,
        shadowRadius: elevation
    };
}
const styles = StyleSheet.create({
    header: {
        zIndex: 1,
        height: 50,
        justifyContent: 'center',
        backgroundColor: '#C4C4C4',
        ...elevationShadowStyle(4)
    },
    headerText: {
        width: 130,
        fontSize: 24,
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center',
        lineHeight: 28,
    },
})