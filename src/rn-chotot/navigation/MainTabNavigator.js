import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import SearchScreen from '../screens/SearchScreen';
import ProfileScreen from '../screens/ProfileScreen';
import Colors from '../constants/Colors';
import SaveScreen from '../screens/ModuleSave/SaveScreen';
import SearchDetail from '../screens/ModuleSearch/SearchDetail';
import DetailScreen from '../screens/DetailScreen';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},

});

const SaveStack = createStackNavigator(
  {
    SaveScreen: {
      screen: SaveScreen,
      navigationOptions: {
        header: null
      }
    }
  }
)

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        header: null
      }
    },
    SaveStack: {
      screen: SaveStack,
      navigationOptions: {
        header: null
      }
    },
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Trang chủ',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={"Home"} />
  )
};

HomeStack.path = '';

const SearchStack = createStackNavigator(
  {
    Search: SearchScreen,
    sc_SearchDetail: SearchDetail
  },
);

SearchStack.navigationOptions = {
  tabBarLabel: 'Tìm',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={"Search"} />
  ),
};

SearchStack.path = '';

const ProfileStack = createStackNavigator(
  {
    Profile: ProfileScreen,
  },
  config
);

ProfileStack.navigationOptions = {
  tabBarLabel: 'Hồ sơ',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={"Profile"} />
  )
};
ProfileStack.path = '';


const BottomTabNavigator = createBottomTabNavigator({
  HomeStack,
  SearchStack,
  ProfileStack,
}, {

  tabBarOptions: {
    activeTintColor: '#e91e63',
    labelStyle: {
      fontSize: 10,
      color: Colors.tabBarLabel,
      fontWeight: 'normal'
    }
  }
});

BottomTabNavigator.path = '';

const ModalStack = createStackNavigator(
  {
    BottomTabNavigator,
    DetailScreen: DetailScreen,

  }, {
  ...config,
  mode: 'modal',
  headerMode: 'none',
  transitionConfig: () => ({
    containerStyle: {
      backgroundColor: 'transparent'
    }
  }),
  transparentCard: true,
  cardStyle: {
    backgroundColor: 'transparent',
    opacity: 1
  }
}

);
ModalStack.path = '';

export default ModalStack;
