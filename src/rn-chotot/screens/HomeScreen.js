import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';

import HeaderComponent from '../components/HeaderComponent';
import styles from '../constants/Layout';


export default class HomeScreen extends React.Component {



  renderItemRecent = (item, index, data) => {
    var { navigation } = this.props;

    return (
      <TouchableOpacity activeOpacity={0.5}
        onPress={() => navigation.navigate('DetailScreen')}>
        <View style={{
          marginLeft: index == 0 ? 17 : 0,
          marginRight: index == data.length - 1 ? 17 : 12,
          backgroundColor: '#C4C4C4', borderRadius: 8, width: 141, height: 181
        }}>
        </View>
      </TouchableOpacity>
    )
  }

  renderItemSave = (item, index, data) => {
    return (
      <TouchableOpacity activeOpacity={0.5}>
        <View style={{
          marginLeft: index == 0 ? 17 : 0,
          marginRight: index == data.length - 1 ? 17 : 8,
          backgroundColor: '#C4C4C4', borderRadius: 8, width: 115, height: 115
        }}>
        </View>
      </TouchableOpacity>
    )
  }

  renderItemCare = (item, index, data) => {
    return (
      <TouchableOpacity activeOpacity={0.5}>
        <View style={{
          marginBottom: 30, marginLeft: 17, marginRight: 15,
          backgroundColor: '#C4C4C4', borderRadius: 8, flex: 1, height: 180
        }}>
        </View>
      </TouchableOpacity>
    )
  }


  render() {
    var dataTmp = 'dangquangdong'.split('');

    return (
      <View style={styles.container}>
        <HeaderComponent
          title="Chợ Tốt" />
        <ScrollView
          style={{ flex: 1, zIndex: 0 }}>
          <View style={{ backgroundColor: '#FBEFB0', height: 148 }} />
          <View style={styles.viewSeeAll}>
            <Text style={styles.textSee}>
              {'Xem gần đây'}
            </Text>
            <TouchableOpacity activeOpacity={0.5}>
              <View style={styles.btnSeeAll}>
                <Text style={styles.textSeeAll}>
                  {'Xem tất cả'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={dataTmp}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => this.renderItemRecent(item, index, dataTmp)}
          />


          <View style={styles.viewSeeAll}>
            <Text style={styles.textSee}>
              {'Đã lưu gần đây'}
            </Text>
            <TouchableOpacity activeOpacity={0.5}
              onPress={() => this.props.navigation.navigate('SaveScreen')}>
              <View style={styles.btnSeeAll}>
                <Text style={styles.textSeeAll}>
                  {'Xem tất cả'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={dataTmp}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => this.renderItemSave(item, index, dataTmp)}
          />


          <View style={styles.viewSeeAll}>
            <Text style={styles.textSee}>
              {'Đáng quan tâm'}
            </Text>
            {/* <View style={styles.btnSeeAll}>
              <Text style={styles.textSeeAll}>
                {'Xem tất cả'}
              </Text>
            </View> */}
          </View>
          <FlatList
            showsHorizontalScrollIndicator={false}
            data={dataTmp}
            scrollEnabled={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => this.renderItemCare(item, index, dataTmp)}
          />

        </ScrollView>
      </View>
    );
  }
}

