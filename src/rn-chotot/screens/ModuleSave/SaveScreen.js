import React from 'react';
import {
    View, FlatList, StyleSheet, TouchableOpacity,
    Dimensions
} from 'react-native';
import HeaderComponent from '../../components/HeaderComponent';
import styles from '../../constants/Layout';
const width = Dimensions.get('window').width;
export default class SaveScreen extends React.Component {

    renderItemSave = (item, index) => {
        return (
            <TouchableOpacity activeOpacity={0.5}>
                <View style={stylesSave.viewItemSave}>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        var dataTmp = 'dangquangdong'.split('');

        return (
            <View style={styles.container}>
                <HeaderComponent
                    title="Đã lưu" />
                <View style={{ flex: 1, zIndex: 0 }}>
                    <View style={{ flexDirection: 'row' }}>

                    </View>
                    <FlatList
                        style={{ flex: 1 }}
                        data={dataTmp}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        numColumns={2}
                        renderItem={({ item, index }) => this.renderItemSave(item, index)}
                    />
                </View>
            </View>
        );
    }
}

const stylesSave = StyleSheet.create({
    viewItemSave: {
        //flex: 1,
        marginTop: 11,
        width: width * 0.46,
        height: width * 0.56,
        backgroundColor: '#C4C4C4',
        borderRadius: 8,
        marginHorizontal: 6,
    }
})