import React from 'react';
import {
    View, FlatList, StyleSheet, TouchableOpacity,
    Dimensions, Image, Text
} from 'react-native';
import styles from '../constants/Layout';
import colors from '../constants/Colors';
import Call from '../assets/svg/call.svg';
import Sms from '../assets/svg/sms.svg';
import UnSave from '../assets/svg/unSave.svg';
import Save from '../assets/svg/save.svg';
const width = Dimensions.get('window').width;
export default class DetailScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userSave: false
        }
    }

    render() {
        var { navigation } = this.props;
        var { userSave } = this.state;
        return (
            <View
                style={[styles.container, {
                    justifyContent: 'center',
                    backgroundColor: 'rgba(0,0,0,0.11)',
                }]}>
                <View
                    onTouchEnd={() => navigation.goBack()}
                    style={{ position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }} />
                <View style={{
                    backgroundColor: colors.white, height: '60%',
                    marginBottom: '30%',
                    borderRadius: 6, marginHorizontal: 13,
                }}>
                    <View style={{
                        width: '100%', height: width * 0.5,
                        borderRadius: 6
                    }}>
                        <Image style={{
                            width: '100%', height: width * 0.5,
                            borderRadius: 6
                        }}
                            resizeMode={'cover'}
                            source={require('../assets/images/imageDefault.png')} />
                        <View style={{
                            position: 'absolute', right: 0,
                            bottom: 0, backgroundColor: colors.yellow,
                            borderRadius: 6,
                        }}>
                            <Text style={{
                                fontSize: 18, fontWeight: '500',
                                color: colors.noticeText, paddingVertical: 10, paddingHorizontal: 15
                            }}>
                                {'-5%'}
                            </Text>
                        </View>
                    </View>
                    <View style={{ padding: 10, paddingRight: 12 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text
                                numberOfLines={2}
                                style={[styles.titleHeight, { flex: 1, marginLeft: 8 }]}>
                                {'Mazda CX5 Premium 2018 - trả trước 300tr nhận xe'}
                            </Text>
                            <TouchableOpacity
                                onPress={() => this.setState({ userSave: !userSave })}
                                activeOpacity={0.5}
                                style={[styles.shadow, { marginLeft: 12 }]}>
                                {userSave == true ? <Save
                                    width={40}
                                    height={40} /> :
                                    <UnSave
                                        width={40}
                                        height={40} />}
                            </TouchableOpacity>
                        </View>
                        <Text
                            numberOfLines={6}
                            style={[styles.textContentDetail, { fontStyle: 'italic', marginTop: 12 }]}>
                            {'Chỉ còn vài chiếc màu đỏ cho anh chị nào yêu thích với giá siêu ưu đãi. Hỗ trợ trả góp cao với lãi suất cực thấp Tặng ngay gói bảo dưỡng miễn phí trong vòng 3 năm hoặc 50.000km. Gọi ngay cho em để biết thêm chi tiết về xe. Chỉ còn vài chiếc màu đỏ cho anh chị nào yêu thích với giá siêu ưu đãi. Hỗ trợ trả góp cao với lãi suất cực thấp Tặng ngay gói bảo dưỡng miễn phí trong vòng 3 năm hoặc 50.000km. Gọi ngay cho em để biết thêm chi tiết về xe.'}
                        </Text>
                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    style={[styles.shadow, { backgroundColor: colors.btnDetail, borderRadius: 30, paddingHorizontal: 24, paddingVertical: 6 }]}>
                                    <Text style={{
                                        color: colors.white, fontSize: 16, fontWeight: '600',
                                    }}>
                                        {'Chi tiết'}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                            </View>
                        </View>

                    </View>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={[styles.shadow, stDetail.btnCall, { left: 0 }]}>
                        <Sms
                            width={60}
                            height={60} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={[styles.shadow, stDetail.btnCall, { right: 0 }]}>
                        <Call
                            width={60}
                            height={60} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const stDetail = StyleSheet.create({
    btnCall: {
        position: 'absolute',
        bottom: -72,
        borderRadius: 30,
    }
})
