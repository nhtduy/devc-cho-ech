import React from 'react';
import { ScrollView, StyleSheet, TextInput, View, Image, Dimensions, Text, FlatList, TouchableOpacity } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import styles from '../constants/Layout';
import { Ionicons } from '@expo/vector-icons';
import Search from '../assets/svg/search.svg';
const { width, height } = Dimensions.get("window");
export default class SearchScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  renderItem = (item, index) => {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('sc_SearchDetail')} style={{ paddingTop: 10, marginHorizontal: 17, marginVertical: index == 0 ? null : 10 }} activeOpacity={0.5}>
        <View style={{
          borderRadius: 8, width: '100%', height: height * 0.09,
          backgroundColor: '#C4C4C4',
        }}>
        </View>
      </TouchableOpacity >
    )
  }
  render() {
    return (
      // <ScrollView style={[styles.contain]}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={[stSearch.containLinebot, {
          marginHorizontal: 10, flexDirection: 'row', borderColor: 'gray', borderWidth: 1,
          padding: 10, borderRadius: 4, backgroundColor: 'white'
        }]}>
          <Search width={16} height={16} />
          <TextInput
            keyboardType={'numeric'}
            ref={refs => this.inputSearch = refs}
            // onChangeText={keyWord => {
            // }}
            style={[stSearch.ntextinput, { flex: 1 }]}
            placeholder='Tìm kiếm' >{''}</TextInput>
        </View>
        <View style={[stSearch.shadow, { backgroundColor: 'white', paddingHorizontal: 40, paddingVertical: 10, alignItems: 'center' }]}>
          <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
            Tất cả các ngành hàng
          </Text>
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={[1, 2, 3, 4, 5, 6]}
          renderItem={({ item, index }) => this.renderItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const stSearch = StyleSheet.create({
  containLinebot: {
    borderColor: '#1b3ca6',
    borderBottomWidth: 0.5,
    paddingVertical: 5,
    marginVertical: 8,
    alignItems: 'center'
  },
  shadow: {
    elevation: 6,
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 6,
    shadowOpacity: 0.2,
    shadowColor: 'rgba(0,0,0,0.5)',
  },
  ntextinput: {
    color: 'black',
    fontWeight: '400',
    fontSize: 14,
    paddingVertical: 4,
    paddingHorizontal: 10
  },
})