# DevC Cho Ech
Chợ Ếch

# rn-chotot
A. Cài đặt môi trường
Nếu đã cài đặt expo và đăng nhập expo trên máy rồi thì bỏ qua bước này.
    1/ Install NodeJS 🎰
    Get started with Node to use everything in the JS ecosystem, including Expo and React Native. We recommend using the latest Node version.

    https://nodejs.org/en/download/
    After intall npm success. Then open the terminal (For Mac or Linux) or open cmd if you are using Window

    Then we check version of npm

    npm --version
    Hope you see something like this
    v6.4.1 or vx.x.x
    2/ Create Expo Account ⚡
    3/ Install Expo Cli (Expo Command Line Tool) 💻
    You will run this tool locally to package, serve, and publish your projects.

    npm install expo-cli --global
    4/ Create your first project
    4.1/ Open terminal (Mac or Linux) or cmd - Command line tool (Window)
    4.2/ Login expo using this command
    expo login
    Then type user name and password

B. Chạy rn-chotot
    1/Copy lệnh ở dưới cho lần đầu
        cd src/rn-chotot && npm i && yarn start
    2/ Nếu đã chạy "npm i" trong "src/rn-chotot" rồi thì copy lệnh này
        cd src/rn-chotot && yarn start

C. We have 3 things in the image
    1/ Run on Android device/emulator (https://docs.expo.io/versions/latest/workflow/android-studio-emulator/)
    2/ Run on iOS simulator (Just for Mac, Can't run on Window or Linux)
    3/ Use to run on your device (Android & iOS). At first you need to download on Store
        iOS (AppStore) -- https://search.itunes.apple.com/WebObjects/MZContentLink.woa/wa/link?path=apps%2fexponent
        Android (Playstore) -- https://play.google.com/store/apps/details?id=host.exp.exponent